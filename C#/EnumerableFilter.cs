using System;
using System.Collections.Generic;

namespace IEnumerableExtras
{
    /// <summary>
    /// Provides <see cref="IEnumerable{T}"/> Filter extension.
    /// </summary>
    public static class EnumerableFilter
    {
        /// <summary>
        /// Filter out those items in this collections that don't satisfy <paramref name="filterFunc"/>.
        /// </summary>
        /// <typeparam name="T">/typeparam>
        /// <param name="collection"></param>
        /// <param name="filterFunc"></param>
        /// <returns></returns>
        public static IEnumerable<T> Filter< T >( this IEnumerable<T> collection, Func<T, bool> filterFunc )
        {
            foreach ( T item in collection )
            {
                if ( filterFunc( item ) )
                {
                    yield return item;
                }
            }
        }
    }
}