﻿namespace IEnumerableExtras
{
    /// <summary>
    /// Represents a pair of items of type <typeparamref name="TValue"/>.
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class Pair< TValue >
    {
        private readonly TValue _left;
        private readonly TValue _right;

        /// <summary>
        /// Initialize an instance of <see cref="Pair{TValue}"/> with two
        /// items of type <typeparamref name="TValue"/>.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        public Pair( TValue left, TValue right )
        {
            _left = left;
            _right = right;
        }

        /// <summary>
        /// Gets first (left) item.
        /// </summary>
        public TValue Left
        {
            get
            {
                return _left;
            }
        }

        /// <summary>
        /// Gets second (right) item.
        /// </summary>
        public TValue Right
        {
            get
            {
                return _right;
            }
        }
    }
}