﻿using System;
using System.Collections.Generic;

namespace IEnumerableExtras
{
    public static class ListIndexOf
    {
        /// <summary>
        /// Return index of first element of this list satisfying filter function or -1.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="filterFunc"></param>
        /// <returns></returns>
        public static int IndexOf< T >( this IList<T> list, Func<T, bool> filterFunc )
        {
            return IndexOf( list, filterFunc, 1 );
        }

        /// <summary>
        /// Return index of first element of this list satisfying filter function or -1.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="quant">Quntifier, index will be divisible by this number</param>
        /// <param name="filterFunc"></param>
        /// <returns></returns>
        public static int IndexOf< T >( this IList<T> list, Func<T, bool> filterFunc, int quant )
        {
            if ( quant <= 0 )
            {
                throw new ArgumentException( "Qunatifier must be greater than zero.", "quant" );
            }

            int index = -1;
            for ( int i = 0; i < list.Count; i += quant )
            {
                if ( filterFunc( list[ i ] ) )
                {
                    index = i;
                    break;
                }
            }

            return index;
        }
    }
}