﻿using System;
using System.Collections.Generic;

namespace IEnumerableExtras
{
    /// <summary>
    /// Provides <see cref="IEnumerable{T}"/> Some/All extension.
    /// </summary>
    public static class EnumerableAll
    {
        /// <summary>
        /// Check if all of this collection items satisfy <paramref name="testFunc"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="testFunc"></param>
        /// <returns></returns>
        public static bool All< T >( this IEnumerable<T> collection, Func<T, bool> testFunc )
        {
            foreach ( T enumerable in collection )
            {
                if ( !testFunc( enumerable ) )
                {
                    return false;
                }
            }

            return true;
        }
    }
}