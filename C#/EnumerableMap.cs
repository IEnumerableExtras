﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace IEnumerableExtras
{
    /// <summary>
    /// <see cref="IEnumerable{T}"/> map extension.
    /// </summary>
    public static class EnumerableMap
    {
        /// <summary>
        /// Produce new instance of <see cref="IEnumerable{TResult}"/> by applying
        /// predicate to each element in this instance of <see cref="IEnumerable"/>.
        /// </summary>
        /// <typeparam name="TResult">Resulting collection item type.</typeparam>
        /// <param name="collection">This instance of <see cref="IEnumerable{T}"/>.</param>
        /// <param name="predicate">Function returnting objects of type <typeparamref name="TResult"/> based on objects of type <typeparamref name="T"/>.</param>
        /// <returns></returns>
        public static IEnumerable<TResult> Map< TResult >( this IEnumerable collection, Func<object, TResult> predicate )
        {
            foreach ( var item in collection )
            {
                yield return predicate( item );
            }
        }

        /// <summary>
        /// Produce new instance of <see cref="IEnumerable{TResult}"/> by applying
        /// predicate to each element in this instance of <see cref="IEnumerable{T}"/>.
        /// </summary>
        /// <typeparam name="T">This collection's item type.</typeparam>
        /// <typeparam name="TResult">Resulting collection item type.</typeparam>
        /// <param name="collection">This instance of <see cref="IEnumerable{T}"/>.</param>
        /// <param name="predicate">Function returnting objects of type <typeparamref name="TResult"/> based on objects of type <typeparamref name="T"/>.</param>
        /// <returns></returns>
        public static IEnumerable<TResult> Map< T, TResult >( this IEnumerable<T> collection, Func<T, TResult> predicate )
        {
            foreach ( var item in collection )
            {
                yield return predicate( item );
            }
        }

        public static IEnumerable<TResult> Map< T, TResult >( this IEnumerable<T> collection, Func<T, int, TResult> predicate )
        {
            int i = 0;
            foreach ( var item in collection )
            {
                yield return predicate( item, i++ );
            }
        }
    }
}