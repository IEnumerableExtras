﻿using System.Collections.Generic;

namespace IEnumerableExtras
{
    /// <summary>
    /// Provedes extension method for <see cref="IDictionary{TKey,TValue}"/> to convert dictionary to an array.
    /// </summary>
    public static class DictionaryToArray
    {
        /// <summary>
        /// Convert this instance of <see cref="IDictionary{T,T}"/> to a rectangular
        /// arry of type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">Type for both keys and values of dictionary.</typeparam>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public static T[,] ToArray< T >( this IDictionary<T, T> dictionary )
        {
            T[,] result = new T[dictionary.Count,2];
            int i = 0;

            foreach ( KeyValuePair<T, T> pair in dictionary )
            {
                result[ i, 0 ] = pair.Key;
                result[ i, 1 ] = pair.Value;

                ++i;
            }

            return result;
        }

        /// <summary>
        /// Convert collection of <see cref="KeyValuePair{TKey,TValue}"/> into a <see cref="IDictionary{TKey,TValue}"/>.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> ToDictionary< TKey, TValue >( this IEnumerable<KeyValuePair<TKey, TValue>> collection )
        {
            var dict = new Dictionary<TKey, TValue>();

            foreach ( var item in collection )
            {
                dict.Add( item.Key, item.Value );
            }

            return dict;
        }
    }
}