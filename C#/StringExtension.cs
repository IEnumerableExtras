﻿using System;
using System.Collections.Generic;

namespace IEnumerableExtras
{
    /// <summary>
    /// Provides extension method to split a <see cref="string"/> while keeping the separator.
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Split string using given <paramref name="separator"/>.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string[] Split( this string str, string separator )
        {
            return Split( str, separator, false );
        }

        /// <summary>
        /// Split string using given <paramref name="separator"/>. Remove empty entries 
        /// id <paramref name="removeEmpty"/> is set.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="separator"></param>
        /// <param name="removeEmpty"></param>
        /// <returns></returns>
        public static string[] Split( this string str, string separator, bool removeEmpty )
        {
            return str.Split( new[] {separator}, removeEmpty ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None );
        }

        /// <summary>
        /// Split this string keeping separator in the result.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<string> SplitAround( this string str, string value )
        {
            List<string> res = new List<string>();
            int l = value.Length;
            int n = str.Length;
            int i = 0;

            do
            {
                int j = str.IndexOf( value, i == 0 ? 0 : i + 1 );

                if ( j != -1 )
                {
                    if ( j != i )
                    {
                        res.Add( str.Substring( i, j - i ) );
                    }

                    res.Add( str.Substring( j, l ) );
                    i = j + l;
                }
                else
                {
                    res.Add( str.Substring( i ) );
                    break;
                }
            }
            while ( i < n );

            return res;
        }
    }
}