﻿using System.Collections.Generic;

namespace IEnumerableExtras
{
    /// <summary>
    /// Provides <see cref="IEnumerable{T}"/> with Flatten extension method.
    /// </summary>
    public static class EnumerableFlatten
    {
        /// <summary>
        /// Flatten two-tier collection into one-tier collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static IEnumerable<T> Flatten< T >( this IEnumerable<IEnumerable<T>> collection )
        {
            foreach ( var enumerable in collection )
            {
                if ( enumerable != null )
                {
                    foreach ( var item in enumerable )
                    {
                        yield return item;
                    }
                }
            }
        }
    }
}