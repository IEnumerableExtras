﻿using System;
using System.Collections.Generic;

namespace IEnumerableExtras
{
    /// <summary>
    /// <see cref="IEnumerable{T}"/> for eah extension.
    /// </summary>
    public static class EnumerableForEach
    {
        /// <summary>
        /// Apply <paramref name="action"/> function to each element in this instance of <see cref="IEnumerable{T}"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="action"></param>
        public static void ForEach< T >( this IEnumerable<T> collection, Action<T> action )
        {
            foreach ( var item in collection )
            {
                action( item );
            }
        }
    }
}