﻿using System.Collections.Generic;
using System.Text;

namespace IEnumerableExtras
{
    /// <summary>
    /// Provide <see cref="IEnumerable{T}"/> with Join extension method.
    /// </summary>
    public static class EnumerableJoin
    {
        /// <summary>
        /// Join all members of this <see cref="IEnumerable{T}"/> to a single string using <paramref name="glue"/>
        /// as a separator.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="glue"></param>
        /// <returns></returns>
        public static string Join<T>(this IEnumerable<T> enumerable, string glue)
        {
            return enumerable.Reduce( new StringBuilder(), ( sb, item ) => sb.Length == 0 ? sb.Append( item ) : sb.Append( glue ).Append( item ) ).ToString();
        }
    }
}