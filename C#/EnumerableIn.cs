﻿using System.Collections.Generic;
using System.Linq;

namespace IEnumerableExtras
{
    public static class EnumerableIn
    {
        public static bool In< T >( this T needle, IEnumerable<T> heystack )
        {
            return heystack.Contains( needle );
        }
    }
}