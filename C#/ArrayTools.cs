﻿using System;

namespace IEnumerableExtras
{
    /// <summary>
    /// Helper class containing tools to deal with arrays
    /// </summary>
    public static class ArrayTools
    {
        /// <summary>
        /// Convert a x based array to a 0 based array.
        /// Only works with one or two dimensional arrays.
        /// </summary>
        /// <param name="xBased">'x' based array</param>
        /// <returns>0 based array</returns>
        public static Array ConvertTo0Based( Array xBased )
        {
            Array zeroBasedArray = null;

            // Do nothing if array is null or empty
            if ( xBased != null && xBased.Length > 0 )
            {
                // Get number of dimensions
                int arrayDimensions = xBased.Rank;

                if ( arrayDimensions == 1 )
                {
                    // Code for one dimensional array
                    int ubound1 = xBased.GetUpperBound( 0 );
                    int lbound1 = xBased.GetLowerBound( 0 );

                    zeroBasedArray = Array.CreateInstance( xBased.GetValue( 1 ).GetType(),
                                                           new[] {ubound1 - lbound1 + 1}, new[] {0} );

                    Array.Copy( xBased, lbound1, zeroBasedArray, 0, xBased.Length );
                }
                else if ( arrayDimensions == 2 )
                {
                    // Code for two dimensional arrays
                    int ubound1 = xBased.GetUpperBound( 0 );
                    int lbound1 = xBased.GetLowerBound( 0 );
                    int ubound2 = xBased.GetUpperBound( 1 );
                    int lbound2 = xBased.GetLowerBound( 1 );

                    zeroBasedArray = Array.CreateInstance( xBased.GetValue( 1, 1 ).GetType(),
                                                           new[] {ubound1 - lbound1 + 1, ubound2 - lbound2 + 1},
                                                           new[] {0, 0} );

                    Array.Copy( xBased, lbound1, zeroBasedArray, 0, xBased.Length );
                }
                else
                {
                    throw new ApplicationException( "Cannot rebase arrays of more than 2 dimensions" );
                }
            }

            return zeroBasedArray;
        }


        /// <summary>
        /// Converts a zero based array to a 1 based array
        /// Only works with 1 or 2 dimensional arrays.
        /// </summary>
        /// <param name="zeroBased">Zero based array</param>
        /// <returns>1 based array</returns>
        public static Array ConvertTo1Based( Array zeroBased )
        {
            return ConvertTo1Based( zeroBased, 0, 0 );
        }

        /// <summary>
        /// Converts a zero based array to a 1 based array
        /// Only works with 1 or 2 dimensional arrays.
        /// </summary>
        /// <param name="zeroBased">Zero based array</param>
        /// <param name="di">How many rows to skip.</param>
        /// <returns>1 based array</returns>
        public static Array ConvertTo1Based( Array zeroBased, int di )
        {
            return ConvertTo1Based( zeroBased, di, 0 );
        }

        /// <summary>
        /// Converts a zero based array to a 1 based array
        /// Only works with 1 or 2 dimensional arrays.
        /// </summary>
        /// <param name="zeroBased">Zero based array</param>
        /// <param name="di">How many rows to skip.</param>
        /// <param name="dj">How many columns to skip.</param>
        /// <returns>1 based array</returns>
        public static Array ConvertTo1Based( Array zeroBased, int di, int dj )
        {
            if ( di < 0 )
            {
                throw new ArgumentException( "di must be greater or equal to 0.", "di" );
            }
            if ( dj < 0 )
            {
                throw new ArgumentException( "dj must be greater or equal to 0.", "dj" );
            }

            Array newArray = null;

            if ( zeroBased != null && zeroBased.Length > 0 )
            {
                // Get number of dimensions
                int arrayDimensions = zeroBased.Rank;

                if ( arrayDimensions == 1 )
                {
                    // Code to copy one dimensional arrays
                    int ubound1 = zeroBased.GetUpperBound( 0 ) - di;

                    newArray = Array.CreateInstance( zeroBased.GetType().GetElementType(), new[] {ubound1 + 1},
                                                     new[] {1} );

                    for ( int i = 0; i <= ubound1; ++i )
                    {
                        newArray.SetValue( zeroBased.GetValue( i + di ), i + 1 );
                    }

                    //Array.Copy( zeroBased, Math.Abs( di ), newArray, 1, zeroBased.Length );
                }
                else if ( arrayDimensions == 2 )
                {
                    // Code to copy two dimensional arrays
                    int ubound1 = zeroBased.GetUpperBound( 0 ) - di;
                    int ubound2 = zeroBased.GetUpperBound( 1 ) - dj;

                    Type type = GetArrayType( zeroBased );

                    newArray = Array.CreateInstance( type, new[] {ubound1 + 1, ubound2 + 1}, new[] {1, 1} );


                    for ( int i = 0; i <= ubound1; ++i )
                    {
                        for ( int j = 0; j <= ubound2; ++j )
                        {
                            newArray.SetValue( zeroBased.GetValue( i + di, j + dj ), i + 1, j + 1 );
                        }
                    }

                    //Array.Copy(zeroBased, Math.Abs(di + dj), newArray, 1, zeroBased.Length);
                }
                else
                {
                    throw new ApplicationException( "Cannot rebase arrays of more than 2 dimensions" );
                }
            }

            return newArray;
        }

        private static Type GetArrayType( Array array )
        {
            foreach ( object array1 in array )
            {
                if ( array1 != null )
                {
                    return array1.GetType();
                }
            }

            return null;
        }

        /// <summary>
        /// Get onde dimensional array froma two dimensional one.
        /// </summary>
        /// <param name="array2D"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static double[] Extract1dFrom2d( double[,] array2D, int index )
        {
            int secondDimCount = array2D.GetUpperBound( 1 ) + 1;

            double[] result = new double[secondDimCount];

            for ( int i = 0; i < secondDimCount; i++ )
            {
                result[ i ] = array2D[ index, i ];
            }
            return result;
        }

        /// <summary>
        /// Convert an istance of <see cref="Array"/> to zero based concrete array.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="a"></param>
        /// <returns></returns>
        public static object AsArray< TValue >( Array a )
        {
            return AsArray<TValue>( a, 0, 0 );
        }

        /// <summary>
        /// Convert an istance of <see cref="Array"/> to zero based concrete array.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="a"></param>
        /// <param name="i0">Starting index i</param>
        /// <returns></returns>
        public static object AsArray< TValue >( Array a, int i0 )
        {
            return AsArray<TValue>( a, i0, 0 );
        }

        /// <summary>
        /// Convert an istance of <see cref="Array"/> to zero based concrete array.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="a"></param>
        /// <param name="i0">Starting index i</param>
        /// <param name="j0">Starting</param>
        /// <returns></returns>
        public static object AsArray< TValue >( Array a, int i0, int j0 )
        {
            object res = null;
            if ( a != null )
            {
                switch ( a.Rank )
                {
                    case 2:
                        TValue[,] e =
                            new TValue[Math.Max( a.GetUpperBound( 0 ), 0 ) + i0,Math.Max( a.GetUpperBound( 1 ), 0 ) + j0
                                ];
                        for ( int i = a.GetLowerBound( 0 ); i <= a.GetUpperBound( 0 ); ++i )
                        {
                            for ( int j = a.GetLowerBound( 1 ); j <= a.GetUpperBound( 1 ); ++j )
                            {
                                e[ i - a.GetLowerBound( 0 ) + i0, j - a.GetLowerBound( 1 ) + j0 ] =
                                    ( TValue )a.GetValue( i, j );
                            }
                        }
                        res = e;
                        break;
                    case 1:
                        TValue[] x = new TValue[Math.Max( a.GetUpperBound( 0 ), 0 ) + i0];
                        for ( int i = a.GetLowerBound( 0 ); i <= a.GetUpperBound( 0 ); ++i )
                        {
                            x[ i - a.GetLowerBound( 0 ) + i0 ] = ( TValue )a.GetValue( i );
                        }
                        res = x;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                res = new TValue[0];
            }

            return res;
        }

        /// <summary>
        /// Initializes array elements with <paramref name="value"/>.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="value"></param>
        public static void Initialize( this Array array, object value )
        {
            for ( int i = 0; i < array.GetLength( 0 ); i++ )
            {
                array.SetValue( value, i );
            }
        }
    }
}